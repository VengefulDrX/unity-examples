using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[RequireComponent (typeof(MeshFilter))]
[RequireComponent (typeof(MeshRenderer))]
public class PolygonGenerator : MonoBehaviour {

    private static float TWOPI = 360.0f;
    public int sides;
    public float spokeLength;
    private float deltaTheta;

    Vector2[] buildUVS(int vCount) {
        List<Vector2> uvs = new List<Vector2>();
        for (int i = 0; i < vCount; i++) {
            switch (i % 4) {
            case 0:
                uvs.Add(new Vector2(0, 0));
                break;
            case 1:
                uvs.Add(new Vector2(1, 0));
                break;
            case 2:
                uvs.Add(new Vector2(0, 1));
                break;
            case 3:
                uvs.Add(new Vector2(1, 1));
                break;
            }
        }
        return uvs.ToArray();
    }

    int[] buildTriangles() {

        int v0, v1, v2 = 0;
        List<int> vn = new List<int>();
        for (int i = 1; i <= sides; i++) {
            v0 = 0;
            v1 = i;
            v2 = i + 1 <= sides ? i + 1 : 1;
            vn.Add(v0);
            vn.Add(v1);
            vn.Add(v2);
        }
        return vn.ToArray();
    }

    
    void Start() {
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        Mesh mesh = new Mesh();
        meshFilter.mesh = mesh;

        deltaTheta = TWOPI / sides;
        Vector3 refVector = Vector3.up * spokeLength;

        VectorList verts = new VectorList();

        verts.Add(transform.position);

        for (int i = 0; i < sides; i++) {
            verts.Add(
             (Quaternion.Euler(0, 0, deltaTheta * i) * refVector)
             + transform.position);
        }

        mesh.vertices = verts.ToArray();
        mesh.uv = buildUVS(verts.Count);
        mesh.triangles = buildTriangles();

        renderer.material = new Material(Shader.Find("Diffuse"));

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        mesh.Optimize();
    }

    void FixedUpdate() {
        MeshFilter mf = GetComponent<MeshFilter>();
        Vector3[] verticies = mf.mesh.vertices;
        Vector3 v0 = verticies[0];
        v0.Set(v0.x, v0.y, Mathf.Sin(Time.time) * spokeLength);

        verticies[0] = v0;

        mf.mesh.vertices = verticies;

        mf.mesh.RecalculateNormals();
        mf.mesh.RecalculateBounds();
        mf.mesh.Optimize();
    }

}
