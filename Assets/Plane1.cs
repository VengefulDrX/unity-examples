using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;

public class VectorList : List<Vector3> {
};

public class VertexList : List<int> {
};

public class UVSList : List<Vector2> {
};

[RequireComponent (typeof(MeshFilter))]
[RequireComponent (typeof(MeshRenderer))]
public class Plane1 : MonoBehaviour {

    public int strips;
    public int stripLength;
    public int stripWidth;

    /// <summary>
    /// Make some wild assumptions about how we want textures
    /// mapped onto the triangles. This is more so unity doesn't complain
    /// than function.
    /// </summary>
    Vector2[] buildUVS(int vCount) {

        UVSList uvs = new UVSList();

        for (int i = 0; i < vCount; i++) {
            switch (i % 4) {
            case 0:
                uvs.Add(new Vector2(0, 0));
                break;
            case 1:
                uvs.Add(new Vector2(1, 0));
                break;
            case 2:
                uvs.Add(new Vector2(0, 1));
                break;
            case 3:
                uvs.Add(new Vector2(1, 1));
                break;
            }
        }
        return uvs.ToArray();
    }

    void buildStripDown(int stripNumber, out VertexList verticies) {
        verticies = new VertexList();

        // It makes more sense to construct a strip with rectangles/squares.
        // Since it takes two triangles to make a rectangle we multiply eveything
        // by two and build two triangles at a time.
        int triangleCount = stripLength * 2;
        int vertexStart = stripNumber * triangleCount;
        int vertexEnd = vertexStart + triangleCount;


        for (int i = vertexEnd-1; i >= vertexStart; i-=2) {
            // The order in which verts are added is important. When RecalculateNormals()
            // is called later, the order of the verticies establishes the righthanded
            // or left handedness of the triangle, its normal vector, and, thus, from
            // which direction it is viewable. I am going for normals in the +Y direction.

            // Triangle 1
            // i-1
            // **********i
            //  *       *
            //    *     *
            //      *   *
            //        * *
            //          * i-2
            verticies.Add(i);
            verticies.Add(i - 1);
            verticies.Add(i - 2);

            // Triangle 2
            // i-1
            //  *
            //  *  *
            //  *    *
            //  *      *
            // i-3********* i-2
            verticies.Add(i - 1);
            verticies.Add(i - 3);
            verticies.Add(i - 2);



        }
    }

    void buildStripUp(int stripNumber, out VertexList verticies) {
        verticies = new VertexList();
        // It makes more sense to construct a strip with rectangles/squares.
        // Since it takes two triangles to make a rectangle we multiply eveything
        // by two and build two triangles at a time.
        int triangleCount = stripLength * 2;

        int vertexStart = stripNumber * triangleCount;
        int vertexEnd = vertexStart + triangleCount;


        for (int i = vertexStart; i < vertexEnd; i+=2) {
            // The order in which verts are added is important. When RecalculateNormals()
            // is called later, the order of the verticies establishes the righthanded
            // or left handedness of the triangle, its normal vector, and, thus, from
            // which direction it is viewable. I am going for normals in the +Y direction.

            // Triangle 1
            // i+2
            //  *
            //  *  *
            //  *    *
            //  *      *
            // i********* i+1
            verticies.Add(i + 2);
            verticies.Add(i + 1);
            verticies.Add(i);

            // Triangle 2
            // i+2
            // **********i+3
            //  *       *
            //    *     *
            //      *   *
            //        * *
            //          * i+1
            verticies.Add(i + 1);
            verticies.Add(i + 2);
            verticies.Add(i + 3);

        }
    }

    Func<int, bool> isEven = x => ((x % 2) == 0);

    VertexList buildStripTriangle(int stripNumber) {

        VertexList verticies;

        if (isEven(stripNumber)) {
            buildStripUp(stripNumber, out verticies);
        } else {
            buildStripDown(stripNumber, out verticies);
        }

        return verticies;

    }

    /// <summary>
    /// Generically builds a strip with triangles.
    /// </summary>
    int[] buildTriangles() {
        VertexList verts = new VertexList();
        foreach (int stripNumber in Enumerable.Range(0, strips)) {

            VertexList stripTriangles = buildStripTriangle(stripNumber);
            verts.AddRange(stripTriangles);
        }

        return verts.ToArray();
    }


    /// <summary>
    /// Builds the strip.
    /// </summary>
    VectorList buildStrip(int number) {

        VectorList verts = new VectorList();

        // Our reference position for the base of the current strip.
        // Each base section will by translated by the A*W: Where
        //
        //  ___ ___[_W_]
        // |   |   |   |
        // |   |   |   |
        // |___|___|___|
        //   0   1   2  ... = A
        //
        // A is the strip number
        // W is the width of an individual strip.
        float xTranslation = (stripWidth * number);

        Vector3 pos = new Vector3(
         transform.position.x + xTranslation,
         transform.position.y,
         transform.position.z);


        // Or those less familiar with this pythonic style iteration...
        // for(int i = 0; i < stripLength * 2; i++)
        foreach (int i in Enumerable.Range(0, stripLength*2)) {
            Vector3 v0 = pos;
            Vector3 v1 = pos;

            v0.Set(v0.x, v0.y, v0.z + (i * stripWidth));
            v1.Set(v1.x + stripWidth, v1.y, v1.z + (i * stripWidth));

            verts.Add(v0);
            verts.Add(v1);
        }


        return verts;
    }

    /// <summary>
    /// Build the list of verticies as component strips.
    /// </summary>
    Vector3[] buildVerts() {
        VectorList verts = new VectorList();

        foreach (int stripNumber in Enumerable.Range(0, strips)) {

            VectorList stripVerticies = buildStrip(stripNumber);
            verts.AddRange(stripVerticies);
        }

        return verts.ToArray();
    }

    void Start() {

        MeshFilter meshFilter = GetComponent<MeshFilter>();
        Mesh mesh = new Mesh();
        meshFilter.mesh = mesh;

        mesh.vertices = buildVerts();
        mesh.uv = buildUVS(mesh.vertexCount);
        mesh.triangles = buildTriangles();

        renderer.material = new Material(Shader.Find("Diffuse"));
        renderer.material.color = new Color(0, 150, 205);

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        mesh.Optimize();
    }



}
