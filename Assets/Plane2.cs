using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;
using PlaneTools;



[RequireComponent (typeof(MeshFilter))]
[RequireComponent (typeof(MeshRenderer))]
public class Plane2 : MonoBehaviour {

    public int columns;
    public int rows;
    public float columnWidth;
    public float rowWidth;
    private Mesh mesh;
    void Start() {

        MeshFilter meshFilter = GetComponent<MeshFilter>();
        mesh = new Mesh();
        meshFilter.mesh = mesh;
        Grid grid = new Grid(
            columns, rows, columnWidth, rowWidth, transform.position);

        mesh.vertices = grid.Verticies;
        mesh.uv = MeshUtilities.BuildUVS(mesh.vertexCount);
        int[] triangles = grid.GetTriangles();
        mesh.triangles = triangles;
        renderer.material = new Material(Shader.Find("Diffuse"));
        renderer.material.color = new Color(0, 150, 205);

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        mesh.Optimize();

    }

    void OnDrawGizmos(){
        Gizmos.color = Color.red;
        foreach(Vector3 vertex in new Grid(3,3,10,10,Vector3.zero).Verticies){
            Gizmos.DrawSphere(vertex, 1.0f);
        }
    }

}
