namespace PlaneTools {

using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;

public struct Point {
    public int x;
    public int z;

    public Point(int _x, int _z) {
        x = _x;
        z = _z;

    }
}




public class Grid {

    int columns;
    int rows;
    float columnWidth;
    float rowWidth;
    Vector3 origin;
    VectorList verticies = null;

    public Grid(int col, int row, float cw, float rw, Vector3 o) {
        columns = col + 1;
        rows = row + 1;
        columnWidth = cw;
        rowWidth = rw;
        origin = o;
    }

    public Vector3[] Verticies {
        get {

            if (verticies != null) {
                goto SkipVertexInit;
            }

            verticies = new VectorList();

            float yCoord = origin.y;

            for (int z = 0; z < rows; z++) {
                float zTranslation = z * rowWidth;
                float zCoord = origin.z + zTranslation;

                for (int x = 0; x < columns; x++) {
                    float xTranslation = x * columnWidth;
                    float xCoord = origin.x + xTranslation;

                    verticies.Add(
                        new Vector3(xCoord, yCoord, zCoord));
                }
            }

        SkipVertexInit:
            return verticies.ToArray();
        }
    }

    private IEnumerable<Point> genPoints() {
        for (int z = 0; z< rows-1; z++) {
            for (int x = 0; x < columns - 1; x++) {
                yield return new Point(x, z);
            }
        }
    }

    public int[] GetTriangles() {
        VertexList triangleVerts = new VertexList();
        foreach (Point p in genPoints()) {
            int i = p.x;
            int k = p.z;

            triangleVerts.Add(IndexOf(i + 1, k));
            triangleVerts.Add(IndexOf(i + 1, k + 1));
            triangleVerts.Add(IndexOf(i, k));

            triangleVerts.Add(IndexOf(i,k));
            triangleVerts.Add(IndexOf(i+1, k+1));
            triangleVerts.Add(IndexOf(i, k+1));
        }
        return triangleVerts.ToArray();
    }

    /// <summary>
    /// 2D point to 1D index;
    /// </summary>
    public int IndexOf(int x, int z) {
        return (z * columns) + x;
    }

}

public static class MeshUtilities {
    /// <summary>
    /// Make some wild assumptions about how we want textures
    /// mapped onto the triangles. This is more so unity doesn't complain
    /// than function.
    /// </summary>
    public static Vector2[] BuildUVS(int vCount) {

        UVSList uvs = new UVSList();

        for (int i = 0; i < vCount; i++) {
            switch (i % 4) {
            case 0:
                uvs.Add(new Vector2(0, 0));
                break;
            case 1:
                uvs.Add(new Vector2(1, 0));
                break;
            case 2:
                uvs.Add(new Vector2(0, 1));
                break;
            case 3:
                uvs.Add(new Vector2(1, 1));
                break;
            }
        }
        return uvs.ToArray();
    }

}

}
